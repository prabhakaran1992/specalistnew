import React, { Component } from "react";
import './index.css';
import './bootstap.css';
import './dark.css';
import { Footer, AutoComplete, Form, message, Popover, Modal, Avatar, Dropdown, Menu, Select, Card, Layout, Cascader, Row, Col, Icon, Button, Tabs, DatePicker, Input, InputNumber } from
  "antd";
import CustomScrollbars from "util/CustomScrollbars";
import { switchLanguage, toggleCollapsedSideNav } from "../../appRedux/actions/Setting";
import SearchBox from "components/SearchBox";
import UserInfo from "components/UserInfo";
import AppNotification from "components/AppNotification";
import MailNotification from "components/MailNotification";
import Testimonials from "../customViews/extras/testimonials/index";
import Callouts from '../customViews/extras/callouts/index'
import RoadMap from "components/Widgets/RoadMap";
import Speacilist from '../../routes/components/dataEntry/Speacilist';
import { Link } from "react-router-dom";
import Lawyer from 'components/Widgets/Lawyer';
import Lawyer1 from "components/Widgets/lawyer1";
import Lawyer2 from 'components/Widgets/lawyer2';
import Lawyer3 from 'components/Widgets/Lawyer3';
import CardBox from "components/CardBox/index";
import Footercontent from "./footer";
import { Carousel } from 'antd';
const { Header } = Layout;
const Option = Select.Option;
const FormItem = Form.Item;
const confirm = Modal.confirm;
function handleMenuClick(e) {
  message.info('Click on menu item.');
}

function handleChange(value) {
  console.log(`selected ${value}`);
}
class SamplePage1 extends Component {
  state = { visible: false, searchText: '' };
  Modal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  showConfirm=() =>{
    confirm({
      title: 'Location not updated!',
      content: 'Please update your current location to see the relevant Specialists around you.',
      okText: 'Enable Location',
      cancelText: 'Update Manually',
      onOk() {
        console.log('Allow');
      },
      onCancel() {
        console.log('Block');
      },
    });
  };



  updateSearchChatUser = (evt) => {
    this.setState({
      searchText: evt.target.value,
    });
  };


  render() {
    const { locale, width, navCollapsed, navStyle } = this.props;
    const menu = (
      <Menu>
        <Menu.Item key="0">
          <li>Lawyer</li>
        </Menu.Item>
        <Menu.Item key="1">
          <li>Sub Category</li>
        </Menu.Item>
      </Menu>
    );
    return (
      <div>
        <div className="gx-header-horizontal ">
          <Modal
            title="Specialist"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Speacilist />
          </Modal>
          <Header
            className="gx-header-horizontal-main">
            <div className="gx-container">
              <div className="gx-header-horizontal-main-flex">

                {/* <div className="gx-d-block gx-d-lg-none gx-linebar gx-mr-xs-3">
                  <i className="gx-icon-btn icon icon-menu"
                    onClick={() => {
                      this.props.toggleCollapsedSideNav(!navCollapsed);
                    }}
                  />

                </div> */}
                {/* <Link to="/" className="gx-d-block gx-d-lg-none gx-pointer gx-w-logo">
                <img alt="" src={require("assets/images/w-logo.png")} /></Link> */}
                <Link to="/" className="image">
                  <img alt="" src={require("assets/images/logo.jpg")} /></Link>
                <div className="gx-header-search gx-d-none gx-d-lg-flex">
                  <SearchBox styleName="gx-lt-icon-search-bar-lg"
                    placeholder="Search in app..."
                    onChange={this.updateSearchChatUser.bind(this)}
                    value={this.state.searchText}
                    style={{ marginLeft: '10px' }} />

                  <Button  style={{ marginLeft: '10px' }} onClick={this.showConfirm}>
                    Detect Location
        </Button>
                  <Link to="list" style={{ marginLeft: '10px' }}><Button type="primary" style={{ width: 120 }}>search</Button></Link>
                </div>

                <ul className="gx-header-notifications gx-ml-auto">
                  <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
                    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={
                      <div className="gx-d-flex">
                        
                        <Link to="list" type="primary"><Button>
                            search
                          </Button></Link>
                        
                        <SearchBox styleName="gx-popover-search-bar"
                          placeholder="Search in app..."
                          onChange={this.updateSearchChatUser.bind(this)}
                          value={this.state.searchText} />
                      </div>
                    } trigger="click">

                      <span className="gx-pointer gx-d-block"><i className="icon icon-search-new" /></span>

                    </Popover>
                  </li>
                  <li className="gx-notify">
                    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight" content={<AppNotification />}
                      trigger="click">
                      <span className="gx-pointer gx-d-block"><i className="icon icon-notification" /></span>
                    </Popover>
                  </li>

                  <li className="gx-msg">
                    <Popover overlayClassName="gx-popover-horizantal" placement="bottomRight"
                      content={<MailNotification />} trigger="click">
                      <span className="gx-pointer gx-status-pos gx-d-block">
                        <i className="icon icon-chat-new" />
                        <span className="gx-status gx-status-rtl gx-small gx-orange" />
                      </span>
                    </Popover>
                  </li>

                  <li className="gx-user-nav"><UserInfo /></li>
                </ul>
              </div>
            </div>
          </Header>
        </div>
        <Carousel autoplay>
          <div>
            <img src={require("assets/images/ca.jpg")} alt='' />
          </div>
          <div>
            <img src={require("assets/images/cb.jpg")} alt='' />
          </div>
          <div>
            <img src={require("assets/images/cc.jpg")} alt='' />
          </div>
          <div>
            <img src={require("assets/images/cd.jpg")} alt='' />
          </div>
        </Carousel>
        <Row>
          <div class="overlay-fade">
            <h2>Lawyer</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider.png")} alt='' />
            <div>
              <img src={require("assets/images/c1.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Teacher</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider1.png")} alt='' />
            <div>
              <img src={require("assets/images/c2.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Accountant</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider2.png")} alt='' />
            <div>
              <img src={require("assets/images/c3.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Finanical</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider3.png")} alt='' />
            <div>
              <img src={require("assets/images/c4.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
          <div class="overlay-fade">
            <h2>Professor</h2>
            <i className="<ItalicOutlined />" />
            <img src={require("assets/images/slider4.png")} alt='' />
            <div>
              <img src={require("assets/images/c5.jpg")} alt='' />
              <a href="#">READ MORE</a>
            </div>
          </div>
         
        </Row>

        <div style={{ marginTop: '50px' }}>
          <CardBox styleName="gx-card-full" heading={'What about today--'}>
            <Testimonials />
          </CardBox>
        </div>
        <div style={{ marginTop: '50px' }}>
          <CardBox styleName="gx-card-full" heading={'Why use Specialist?'}>
            <Callouts />
          </CardBox>
        </div>
        <div style={{ marginTop: '50px' }}>
          <CardBox styleName="gx-card-full" heading={'My recommended Finds of the week - Lawyers'}>
            <Row>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <Lawyer />
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <Lawyer1 />
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <Lawyer2 />
              </Col>
              <Col xl={6} lg={12} md={12} sm={12} xs={24}>
                <Lawyer3 />
              </Col>

            </Row>
          </CardBox >
        </div>
        <div>
          <Footercontent />
        </div>

      </div>
    );
  }
}
const mapStateToProps = ({ settings }) => {
  const { locale, navStyle, navCollapsed, width } = settings;
  return { locale, navStyle, navCollapsed, width }
};
export default SamplePage1;