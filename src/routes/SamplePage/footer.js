import React, { Component } from "react";


class Footercontent extends Component{

    render(){
        return(
            <footer class="site-footer">
          <div class="container">
            <div class="row">
              <div class="col-lg-3">
                <div class="footer-logo footer-widget"><img style={{ width: 50 }} src={require("assets/images/logo.jpg")} alt="Footer logo" /></div>
                <p class="copy-right">Copyright © 2018 G-axon</p></div>
              <div class="col-lg-8">
                <ul class="footer-menu-wrapper">
                  <li>
                    <div class="footer-widget"><h3 class="footer-menu-title">Our Company</h3>
                      <ul class="footer-menu">
                        <li>
                          <a href="#">About Us</a>
                        </li>
                        <li>
                          <a href="#">How It Works</a>
                        </li>
                        <li>
                          <a href="#">Affiliates</a>
                        </li>
                        <li>
                          <a href="#">Testimonials</a>
                        </li>
                        <li>
                          <a href="#">Contact Us</a>
                        </li>
                        <li>
                          <a href="#">Plan &amp; Pricing</a>
                        </li>
                        <li>
                          <a href="#">Blog</a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                  <div class="footer-widget"><h3 class="footer-menu-title">Getting Started</h3>
                      <ul class="footer-menu">
                      <li><a href="#">Contact</a></li>
                      <li><a href="#" >Join Specialist</a></li>
                      <li><a href="#" >Social Media & Blog</a></li>
                      <li><a href="#" >All Practices</a></li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    
                  <div class="footer-widget"><h3 class="footer-menu-title">Help Support</h3>
                      <ul class="footer-menu">
                      <li>
                        <a href="#">Support Forum</a>
                      </li>
                      <li>
                        <a href="#">Terms &amp; Conditions</a>
                      </li>
                      <li>
                        <a href="#">Support Policy</a>
                      </li>
                      <li>
                        <a href="#">Refund Policy</a>
                      </li>
                      <li>
                        <a href="#">FAQs</a>
                      </li>
                      <li>
                        <a href="#">Buyers Faq</a>
                      </li>
                      <li>
                        <a href="#">Sellers Faq</a>
                      </li>
                      </ul>
                    </div>

                  </li>
                  
                </ul>

              </div>

            </div>

          </div>

        </footer>
        )
    }
}
export default Footercontent;