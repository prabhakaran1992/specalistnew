export default [

  
  {
    'id': 3,
    'handle': 'today',
    'title': 'Upcoming',
    'icon': 'calendar'
  },
  {
    'id': 4,
    'handle': 'completed',
    'title': 'History',
    'icon': 'check-circle-o'
  },
  
];
