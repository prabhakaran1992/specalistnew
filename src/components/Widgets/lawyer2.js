import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import RoadMapItem from "./RoadMapItem";
import { lawyerList2 } from "../../routes/main/Widgets/data"
import CardBox from "components/CardBox/index";

class lawyer2 extends Component {

    render() {

        const settings = {
            arrows: false,
            dots: true,
            infinite: true,
            speed: 500,
            marginLeft: 10,
            marginRight: 10,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return (
            <CardBox styleName="gx-card-full" heading={''}>
                <Slider className="gx-slick-slider" {...settings}>
                    <div className="gx-slider">
                        <div className="gx-slider-img">
                            <img alt="example" src={require("assets/images/lawyer3.jpg")} style={{ maxHeight: 185 }} />
                            <img className="gx-img-up" alt="example" src={require("assets/images/law.jpg")} />
                        </div>
                        <div className="gx-slider-content">
                            <h4>Criminal Lawyer</h4>
                            <p className="gx-text-grey">“When an individual spreads rumors about others, it erroneously damage their reputation. ”</p>
                        </div>
                    </div>
                </Slider>
            </CardBox>
        );
    }
}

export default lawyer2;